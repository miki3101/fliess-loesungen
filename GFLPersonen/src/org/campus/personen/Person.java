package org.campus.personen;

public class Person
{
	private String vorname;
	private String nachname;
	private Person mutter;
	private Person vater;

	private static int anzahlPersonen;

	public Person(String vorname, String nachname)
	{
		this.vorname = vorname;
		this.nachname = nachname;
		anzahlPersonen++;
	}

	public void setVater(Person vater)
	{
		this.vater = vater;
	}

	public void setMutter(Person mutter)
	{
		this.mutter = mutter;
	}
	
	public Person getVater()
	{
		return vater;
	}

	public String toString()
	{
		if(mutter == null && vater == null) {
			return String.format("(%s %s (? ?)) %d", vorname, nachname, anzahlPersonen);
		}
		
		if(mutter == null)
		{
			return String.format("(%s %s (? %s)) %d", vorname, nachname, vater, anzahlPersonen);
		}
		
		if(vater == null)
		{
			return String.format("(%s %s (%s ?)) %d", vorname, nachname, mutter, anzahlPersonen);
		}
		
		return String.format("(%s %s (%s %s)) %d", vorname, nachname, mutter, vater, anzahlPersonen);
	}

}
