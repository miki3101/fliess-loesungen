
public class BruttoNetto
{
	public static void main(String[] args)
	{
		double netto = 1123.454;
		double steuer = 20;
		
		double brutto = netto * (1 + steuer / 100);

		String test = "w4tho4 ";
		
		System.out.println(brutto);

		System.out.printf("      netto: %.2f \n", netto); // \n zeilenumbruch
		System.out.printf("     steuer: %.2f \n", steuer); // \n zeilenumbruch
		System.out.printf("Bruttopreis: %.2f \n", brutto); // \n zeilenumbruch

		int a = 4; // = ist zuweiseung!

		if (a == 2) // if braucht immer einen boolean, == vergleich
		{
			System.out.println("a ist gleich 2");

		} else if (a == 3)
		{
			System.out.println("a ist gleich 3");

		}
		else
		{
			System.out.println("a ist nicht gleich 2 und nicht 3");
		}

	}
}
