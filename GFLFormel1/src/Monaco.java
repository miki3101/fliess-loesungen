import Auto.Auto;

public class Monaco
{

	public static void main(String[] args)
	{

		int a1 = 1;
		int a2 = 2;
		int a3 = a1;
		a3 = 3;

		Auto auto1 = new Auto("rot");
		Auto auto2 = new Auto("blau");
		Auto auto3 = auto1;
		
		System.out.println(auto1.toString()); // gleichwertig
		System.out.println(auto1);

		auto1.status();

		auto1.volltanken();
		auto2.volltanken();

		auto1.status();

		auto1.beschleunigen(10);
		auto1.status();

		auto1.fahren(120);
		auto1.status();

		test();

	}

	public static Auto test()
	{
		int index = 0;
		Auto auto1 = new Auto("gr�n");
		return auto1;
	}

}
