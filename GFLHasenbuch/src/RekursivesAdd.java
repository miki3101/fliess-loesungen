
public class RekursivesAdd
{
	public static void main(String[] args)
	{
		System.out.println(add(1));

		System.out.println(add(2));

		System.out.println(add(3));

		System.out.println(add(4));

		System.out.println(add(5));
	}

	public static int add(int zahl)
	{
		if (zahl == 1)
		{
			return 1;
		}

		int result = zahl;
		zahl--;

		result = result + add(zahl);

		return result;
	}
}
