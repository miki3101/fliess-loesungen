import org.campus.uno.Farbe;
import org.campus.uno.Karte;

public class KartenDemo
{
	public static void main(String[] args)
	{
		Karte k1 = new Karte(Farbe.rot, 2);
		Karte k2 = new Karte(Farbe.rot, 1);
		Karte k3 = new Karte(Farbe.gruen, 4);
		Karte k4 = new Karte(Farbe.gruen, 2);
		
		System.out.println(k1);
		
		System.out.println(k1.match(k2));
		System.out.println(k1.match(k3));
		
		System.out.println(k1.compare(k2));
		System.out.println(k1.compare(k4));
		System.out.println(k4.compare(k3));
		
		
		
	}
}
